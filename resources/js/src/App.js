import React, { Component } from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'

import Base from './pages/layout/Base'
import Lateral from './pages/layout/lateral/Lateral'

import Account from './pages/account/Account'
import Product from './pages/product/Product'
import Member from './pages/member/Member'
import Debt from './pages/debt/Debt'

export default class App extends Component {
    render() {
        return (
            <Router>
                <Base>
                    <Lateral/>
                    <Switch>
                        <Route exact path = "/" component = {Account} />
                        <Route path = "/account/:accountId" component = {Member} />
                        <Route exact path = "/member" component = {Debt} />
                        <Route path = "/member/:memberId" component = {Debt} />
                        <Route path = "/product" component = {Product} />
                    </Switch>
                </Base>
            </Router>
        );
    }
}
