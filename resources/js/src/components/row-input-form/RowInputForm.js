import React, { Component } from 'react';

import CustomSvg from './../../components/CustomSvg'
import Color from './../../constants/Color'

import './style.css'

export default class RowInputForm extends Component {

    constructor(props){
        super(props)
        this.state = {
            firstInput : !!this.props.default ? this.props.default : '',
            secondInput : !!this.props.defaultSecond ? this.props.defaultSecond : ''
        }
    }

    render() {
        return(
            <tr>
                <td>{this.props.number}</td>
                <td colSpan = {!!this.props.secondPlaceholder && this.props.secondPlaceholder.length ? "1" : "3"}>
                    <input
                        value = {this.state.firstInput}
                        className = "base__input-default base__input-add custom__default"
                        type = "text"
                        onChange = { (e) => this.setState({firstInput : e.target.value})}
                        placeholder = {this.props.firstPlaceholder}
                    />
                </td>
                {!!this.props.secondPlaceholder && this.props.secondPlaceholder.length &&
                <td>
                    <input
                        value = {this.state.secondInput}
                        className = "base__input-default base__input-add custom__soft"
                        type = "number"
                        step = "0.1"
                        min = "1"
                        onChange = { (e) => this.setState({secondInput : e.target.value})}
                        placeholder = {this.props.secondPlaceholder}
                    />
                </td>
                }
                <td className = "custom__column">
                    <div className = "base__actions-container account__actions-container">
                        <div className = "base__actions-icon"
                            onClick = {() => this.props.onConfirm(this.state.firstInput, this.state.secondInput)}>
                            <CustomSvg
                                name = "confirm"
                                width = {25}
                                height = {25}
                                color  = {Color.orange}
                            />
                        </div>
                        <div className = "base__actions-icon" 
                            onClick = {() => this.props.onCancel()}>
                            <CustomSvg
                                name = "cancel"
                                width = {25}
                                height = {25}
                                color  = {Color.orange}
                            />
                        </div>
                    </div>
                </td>
            </tr>
        )
    }
}

