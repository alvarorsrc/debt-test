import React, { Component } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { loadMembers, deleteMember } from './../../actionCreators'
import CustomSvg from './../../components/CustomSvg'
import Color from './../../constants/Color'
import { normalizeString } from './../../util/functions'

import './style.css'

class Member extends Component {

    constructor(props){
        super(props)

        this.state = {
            filter : '',
            accountId : this.props.match.params.accountId
        }

    }

    componentDidMount(){
        this.props.loadData(this.state.accountId)
    }

    render() {
        let cont = 0
        return (
            <div className = "base__page-container">
                <h1>Miembros</h1>

                <div className = "base__filters-contaier">
                    <input
                        className = "base__input-default"
                        type = "text"
                        onChange = { (e) => this.setState({filter : e.target.value})}
                        placeholder = "Buscar miembro"
                    />

                    <div className = "base__actions-icon" onClick = {() => {
                            this.props.history.push({ pathname: '/member', state: {accountId : this.state.accountId}})
                            
                        }}>
                        <CustomSvg
                            name = "add"
                            width = {25}
                            height = {25}
                            color  = {Color.orange}
                        />
                    </div>
                </div>

                <table>
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Nombre</th>
                        <th>Fecha he creación</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.members.map((member) => {
                        if (this.state.filter.length > 0 && !normalizeString(member.name).includes(normalizeString(this.state.filter))) {
                            return null
                        }
                        return (
                            <tr key = {member.id}>
                                <td>{++cont}</td>
                                <td>{member.name}</td>
                                <td>{member.created_at.substring(0,10)}</td>
                                <td>
                                    <div className = "base__actions-container">
                                        <div className = "base__actions-icon" onClick = {() => {
                                            this.props.history.push({ pathname: '/member/' + member.id, state: {accountId : this.state.accountId, memberName : member.name}})
                                            
                                        }}>
                                            <CustomSvg
                                                name = "edit"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                        <div className = "base__actions-icon" onClick = {() => {
                                            this.props.removeMember(member.id)
                                        }}>
                                            <CustomSvg
                                                name = "delete"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        members : state.members
    }
}

const mapDispatchToProps = dispath => {
    return {
        loadData(accountId){
            dispath(loadMembers(accountId))
        },
        removeMember(memberId){
            dispath(deleteMember(memberId))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Member))