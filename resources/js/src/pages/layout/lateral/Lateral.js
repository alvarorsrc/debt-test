import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './style.css'

export default class Lateral extends Component {
    
    componentDidMount(){
        
    }

    render() {
        return (       
                <div className = "lateral__container">
                    <div>
                        <Link to = "/" >Cuentas</Link>
                    </div>
                    <div>
                        <Link to = "/product" >Productos</Link>
                    </div>
                </div>
        )
    }
}