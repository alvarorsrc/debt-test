import React, { Component } from 'react';
import { connect } from 'react-redux'

import { loadAccountsInPeriod, loadProducts } from './../../actionCreators'

import './style.css'

class Base extends Component {
    
    componentDidMount(){
        this.props.refreshAccounts()
        this.props.refreshProducts()
    }

    render() {
        return (
            <div className = "base__container">
                {this.props.children}
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        
    }
}

const mapDispatchToProps = dispath => {
    return {
        refreshAccounts(){
            dispath(loadAccountsInPeriod())
        },
        refreshProducts(){
            dispath(loadProducts())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Base)