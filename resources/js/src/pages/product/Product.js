import React, { Component } from 'react';
import { connect } from 'react-redux'
import { updateProduct, addProduct, deleteProduct } from './../../actionCreators'

import CustomSvg from './../../components/CustomSvg'
import Color from './../../constants/Color'
import RowInputForm from './../../components/row-input-form/RowInputForm'
import { normalizeString } from './../../util/functions'

import './style.css'

class Product extends Component {

    constructor(props){
        super(props)

        this.state = {
            newProduct: false,
            isEditing : {},
            filter : '',
        }
    }

    componentDidMount(){
        
    }

    render() {
        let cont = 0
        return (
            <div className = "base__page-container">
                <h1>Productos</h1>

                <div className = "base__filters-contaier">
                    <input
                        className = "base__input-default"
                        type = "text"
                        onChange = { (e) => this.setState({filter : e.target.value})}
                        placeholder = "Buscar producto"
                    />

                    <div className = "base__actions-icon" onClick = {() => this.setState({ newProduct : true })}>
                        <CustomSvg
                            name = "add"
                            width = {25}
                            height = {25}
                            color  = {Color.orange}
                        />
                    </div>
                </div>

                <table className = "base__table">
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.state.newProduct &&
                        <RowInputForm
                            number = "#"
                            firstPlaceholder = "Ingrese el nombre"
                            secondPlaceholder = "Ingrese el precio"
                            onConfirm = {(name, price) => {
                                if(name.length * price.length && price > 0){
                                    this.props.saveNewProduct({name, price: parseFloat(price)})
                                    this.setState({newProduct : false})
                                }
                            }}
                            onCancel = {()=>this.setState({newProduct:false})}
                        />
                    }
                    {this.props.products.map((product) => {
                        if (this.state.filter.length > 0 && !normalizeString(product.name).includes(normalizeString(this.state.filter))) {
                            return null
                        }
                        if(this.state.isEditing[product.id]){
                            return(
                                <RowInputForm
                                    key = {product.id}
                                    default = {product.name}
                                    defaultSecond = {product.price}
                                    number = {++cont}
                                    firstPlaceholder = "Ingrese el nombre"
                                    secondPlaceholder = "Ingrese el precio"
                                    onConfirm = {(name, price) => {
                                        if(name.length * price.length && price > 0){
                                            this.props.saveProduct(product.id, {name, price: parseFloat(price)})
                                            this.setState({isEditing : {[product.id] : false}})
                                        }
                                    }}
                                    onCancel = {()=>this.setState({isEditing: {[product.id] : false}})}
                                />
                            )
                        }
                        return (
                            <tr key = {product.id}>
                                <td>{++cont}</td>
                                <td>{product.name}</td>
                                <td>{product.price}</td>
                                <td className = "custom__column">
                                    <div className = "base__actions-container">
                                        <div className = "base__actions-icon" 
                                            onClick = {() => {
                                            this.setState({isEditing : { [product.id] : true}})
                                        }}>
                                            <CustomSvg
                                                name = "edit"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                        <div className = "base__actions-icon" onClick = {() => this.props.removeProduct(product.id)}>
                                            <CustomSvg
                                                name = "delete"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        products : state.products
    }
}

const mapDispatchToProps = dispath => {
    return {
        saveNewProduct(product){
            dispath(addProduct(product))
        },
        saveProduct(productId, product){
            dispath(updateProduct(productId, product))
        },
        removeProduct(productId){
            dispath(deleteProduct(productId))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);