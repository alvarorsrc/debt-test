import React, { Component } from 'react';

import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { addAccount, deleteAccount, editAccount } from './../../actionCreators'
import CustomSvg from './../../components/CustomSvg'
import RowInputForm from './../../components/row-input-form/RowInputForm'
import Color from './../../constants/Color'
import { normalizeString } from './../../util/functions'

import './style.css'

class Account extends Component {

    constructor(props){
        super(props)
        this.state = {
            filter : '',
            newAccount : false,
            accountName : '',
            isEditing: {}
        }
    }
    componentDidMount(){
    }

    render() {
        let cont = 0
        return (
            <div className = "base__page-container">
                <h1>Cuentas</h1>

                <div className = "base__filters-contaier">
                    <input
                        className = "base__input-default"
                        type = "text"
                        onChange = { (e) => this.setState({filter : e.target.value})}
                        placeholder = "Buscar cuenta"
                    />

                    <div className = "base__actions-icon" onClick = {() => this.setState({ newAccount : true })}>
                        <CustomSvg
                            name = "add"
                            width = {25}
                            height = {25}
                            color  = {Color.orange}
                        />
                    </div>
                </div>

                <table className = "base__table">
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Nombre</th>
                        <th>Miembros</th>
                        <th>Fecha he creación</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.state.newAccount &&
                        <RowInputForm
                            number = "#"
                            firstPlaceholder = "Ingrese el nombre de la cuenta"
                            onConfirm = {name => {
                                if(name.length){
                                    this.props.saveNewAccount(name)
                                    this.setState({newAccount : false})
                                }
                            }}
                            onCancel = {()=>this.setState({newAccount:false})}
                        />
                    }
                    {this.props.accounts.map((account) => {
                        if (this.state.filter.length > 0 && !normalizeString(account.name).includes(normalizeString(this.state.filter))) {
                            return null
                        }
                        if(this.state.isEditing[account.id]){
                            return(
                                <RowInputForm
                                    key = {account.id}
                                    default = {account.name}
                                    number = {++cont}
                                    placeholder = "Ingrese el nombre de la cuenta"
                                    onConfirm = {name => {
                                        if(name.length){
                                            this.props.saveAccount(account.id, {name})
                                            this.setState({isEditing : { [account.id] : false}})
                                        }
                                    }}
                                    onCancel = {()=>this.setState({isEditing: { [account.id] : false}})}
                                />
                            )
                        }
                        return (
                            <tr key = {account.id}>
                                <td>{++cont}</td>
                                <td className = "custom__default">{account.name}</td>
                                <td>{account.members_count}</td>
                                <td>{account.created_at.substring(0,10)}</td>
                                <td className = "custom__column">
                                    <div className = "base__actions-container account__actions-container">
                                        <div className = "base__actions-icon" 
                                            onClick = {() => {
                                            this.props.history.push('/account/' + account.id)
                                        }}>
                                            <CustomSvg
                                                name = "view"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                        <div className = "base__actions-icon" 
                                            onClick = {() => {
                                            this.setState({isEditing : { [account.id] : true}})
                                        }}>
                                            <CustomSvg
                                                name = "edit"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                        <div className = "base__actions-icon" onClick = {() => {
                                            this.props.saveAccount(account.id, {finish: true})
                                        }}>
                                            <CustomSvg
                                                name = "finish"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                        <div className = "base__actions-icon" onClick = {() => this.props.removeAccount(account.id)}>
                                            <CustomSvg
                                                name = "delete"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        accounts : state.accountsInPeriod
    }
}

const mapDispatchToProps = dispath => {
    return {
        saveNewAccount(account){
            dispath(addAccount(account))
        },
        removeAccount(id){
            dispath(deleteAccount(id))
        },
        saveAccount(id, account){
            dispath(editAccount(id, account))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Account))