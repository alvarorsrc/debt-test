import React, { Component } from 'react';
import { connect } from 'react-redux'
import axios from 'axios';
import { withRouter } from 'react-router-dom'

import { addMember, updateMember } from './../../actionCreators'
import CustomSvg from './../../components/CustomSvg'
import RowInputForm from './../../components/row-input-form/RowInputForm'
import Color from './../../constants/Color'

import './style.css'

class Debt extends Component {

    constructor(props){
        super(props)

        this.state = {
            memberId : !!this.props.match.params ? this.props.match.params.memberId : 0,
            memberName : !!this.props.location.state ? this.props.location.state.memberName : '',
            accountId: !!this.props.location.state ? this.props.location.state.accountId : 0,
            productQuantity : 0,
            selectedProduct : !!this.props.products[0] ? this.props.products[0].id : 0,
            debts : [],
            isEditing:{},
            showMsg : false
        }
    }

    componentDidMount(){
        if(this.state.memberId){
            axios.get('/api/debt/' + this.state.memberId).then(res => {
                if(res.data.status){
                    this.setState(state => {
                        return {
                            debts : state.debts.concat(res.data.debts)
                        }
                    })
                }
            })
        }
    }

    render() {
        let cont = 0
        return (
            <div className = "base__page-container">
                <h1>Deuda</h1>

                <div className = "base__filters-contaier debt__control-inputs">
                    <input
                        value = {this.state.memberName}
                        className = "base__input-default"
                        type = "text"
                        onChange = { (e) => this.setState({memberName : e.target.value})}
                        placeholder = "Nuevo miembro"
                    />
                    <button onClick = {() => {
                        if(!!this.state.memberName && this.state.memberName.length){
                            if(this.state.memberId)this.props.saveMember( this.state.memberId,{name : this.state.memberName})
                            else this.props.saveNewMember({name : this.state.memberName, account_id : this.state.accountId})
                        }
                    }}>Guardar Miembro</button>
                    {!!this.props.lastMember && this.props.lastMember.id > 0 &&
                    <p>Guardado</p>   
                    }
                        
                </div>

                <div className = "base__filters-contaier debt__control-inputs">
                    <select className = "base__input-default" value = {this.state.selectedProduct} onChange = {e => this.setState({selectedProduct : e.target.value})}>
                        {this.props.products.map(product => {
                            return (
                                <option key = {product.id} value = {product.id}>{product.name}</option>
                            )
                        })}
                    </select>
                    <input
                        className = "base__input-default debt__input-quantity"
                        type = "number"
                        onChange = { (e) => this.setState({productQuantity : e.target.value})}
                        placeholder = "Cantidad"
                    />
                    <div className = "base__actions-icon" onClick = {() => this.addDebt()}>
                        <CustomSvg
                            name = "add"
                            width = {25}
                            height = {25}
                            color  = {Color.orange}
                        />
                    </div>
                </div>

                <table>
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.state.debts.map((debt) => {
                        if(this.state.isEditing[debt.id]){
                            return(
                                <RowInputForm
                                    key = {debt.id}
                                    default = {debt.quantity}
                                    number = {++cont}
                                    firstPlaceholder = "Cantidad de productos"
                                    onConfirm = {(quantity) => {
                                        this.updateDebt(debt.id, {quantity})
                                        this.setState({isEditing : {[debt.id] : false}})
                                    }}
                                    onCancel = {()=> this.setState({isEditing: {[debt.id] : false}})}
                                />
                            )
                        }

                        return (
                            <tr key = {debt.id}>
                                <td>{++cont}</td>
                                <td>{debt.product}</td>
                                <td>{debt.quantity}</td>
                                <td>{debt.quantity * debt.price}</td>
                                <td>
                                    <div className = "base__actions-container">
                                        <div className = "base__actions-icon" 
                                            onClick = {() => {
                                            this.setState({isEditing : { [debt.id] : true}})
                                        }}>
                                            <CustomSvg
                                                name = "edit"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                        <div className = "base__actions-icon" onClick = {() => this.removeDebt(debt.id)}>
                                            <CustomSvg
                                                name = "delete"
                                                width = {25}
                                                height = {25}
                                                color  = {Color.orange}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>

            </div>
        )
    }

    addDebt(){
        if(this.state.selectedProduct && this.state.productQuantity.length && this.state.productQuantity > 0){
            let member_id = this.state.memberId > 0 ? this.state.memberId : this.props.lastMember.id
            axios.post('/api/debt', {member_id, product_id : this.state.selectedProduct, quantity : parseInt(this.state.productQuantity)})
            .then(res => {
                if(res.data.status){
                    this.setState(state => {
                        return {
                            debts: state.debts.concat(res.data.debt),
                        }
                    })
                }
            })

        }
    }

    updateDebt(debtId, debt){
        if(debt.quantity.length && debt.quantity){
            axios.put('/api/debt/' + debtId, debt)
            .then(res => {
                if(res.data.status){
                    this.setState(state => {
                        let index = state.debts.map(item => item.id).indexOf(debtId)
                        if(index >= 0){
                            let newArr = state.debts.slice(0)
                            newArr[index] = res.data.debt
                            return {
                                debts: newArr
                            }
                        }
                    })
                }
            })
        }
    }

    removeDebt(debtId){
        axios.delete('/api/debt/' + debtId)
        .then(res => {
            if(res.data.status){
                this.setState(state => {
                    let index = state.debts.map(item => item.id).indexOf(debtId)
                    if(index >= 0){
                        let newArr = state.debts.slice(0)
                        newArr.splice(index, 1)
                        return {
                            debts: newArr
                        }
                    }
                })
            }
        })
    }
}

const mapStateToProps = state => {
    return {
        products : state.products,
        lastMember : state.lastMember,
    }
}

const mapDispatchToProps = dispath => {
    return {
        saveNewMember(member){
            dispath(addMember(member))
        },
        saveMember(memberId, member){
            dispath(updateMember(memberId, member))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Debt))