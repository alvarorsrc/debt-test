const Color = {
    orange : "#EB6841",
    yellow : "#EDC951",
    red : "#CC2A36",
    cyan : "#03A0B0",
    Brown : "#4F372D",
}

export default Color
