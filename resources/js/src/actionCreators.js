import axios from 'axios'

const loadAccountsInPeriod = () => {
    return dispatch => {
        return axios.get('/api/account').then(res => {
            if(res.data.status){
                dispatch({
                    type : "REFRESH_ACCOUNTS_IN_PERIOD",
                    accounts : res.data.accounts
                })
            }
        })
    }
}

const addAccount = (name) => {
    return dispatch => {
        return axios.post('/api/account', {name}).then(res => {
            if(res.data.status){
                dispatch({
                    type : "ADD_ACCOUNT",
                    account : {...res.data.account, members_count: 0}
                })
            }
        })
    }
}

const editAccount = (accountId, account) => {
    return dispatch => {
        return axios.put('/api/account/' + accountId, account).then(res => {
            if(res.data.status){
                dispatch({
                    type : "UPDATE_ACCOUNT",
                    account : res.data.account
                })
            }
        })
    }
}

const deleteAccount = (accountId) => {
    return dispatch => {
        return axios.delete('/api/account/' + accountId).then(res => {
            if(res.data.status){
                dispatch({
                    type : "DELETE_ACCOUNT",
                    accountId
                })
            }
        })
    }
}

const loadMembers = (accountId) => {
    return dispatch => {
        return axios.get('/api/member/' + accountId).then(res => {
            if(res.data.status){
                dispatch({
                    type : "REFRESH_MEMBERS",
                    members : res.data.members
                })
            }
        })
    }
}

const updateMember = (memberId, member) => {
    return dispatch => {
        return axios.put('/api/member/' + memberId, member).then(res => {
            if(res.data.status){
                dispatch({
                    type : "UPDATE_MEMBER",
                    member : res.data.member
                })
            }
        })
    }
}

const addMember = (member) => {
    return dispatch => {
        return axios.post('/api/member', member).then(res => {
            if(res.data.status){
                dispatch({
                    type : "ADD_MEMBER",
                    member : res.data.member
                })
            }
        })
    }
}

const deleteMember = (memberId) => {
    return dispatch => {
        return axios.delete('/api/member/' + memberId).then(res => {
            if(res.data.status){
                dispatch({
                    type : "DELETE_MEMBER",
                    memberId
                })
            }
        })
    }
}

const loadProducts = () => {
    return dispatch => {
        return axios.get('/api/product').then(res => {
            if(res.data.status){
                dispatch({
                    type : "REFRESH_PRODUCTS",
                    products : res.data.products
                })
            }
        })
    }
}

const addProduct = (product) => {
    return dispatch => {
        return axios.post('/api/product', product).then(res => {
            if(res.data.status){
                dispatch({
                    type : "ADD_PRODUCT",
                    product : res.data.product
                })
            }
        })
    }
}

const updateProduct = (productId, product) => {
    return dispatch => {
        return axios.put('/api/product/' + productId, product).then(res => {
            if(res.data.status){
                dispatch({
                    type : "UPDATE_PRODUCT",
                    product : res.data.product
                })
            }
        })
    }
}

const deleteProduct = (productId) => {
    return dispatch => {
        return axios.delete('/api/product/' + productId).then(res => {
            if(res.data.status){
                dispatch({
                    type : "DELETE_PRODUCT",
                    productId
                })
            }
        })
    }
}

export { addAccount, loadAccountsInPeriod, editAccount, deleteAccount, loadMembers, addMember, updateMember, deleteMember, loadProducts, updateProduct, addProduct, deleteProduct}