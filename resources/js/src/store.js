import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

const reducer = (state, action) => {

    switch(action.type){
        case 'REFRESH_ACCOUNTS_IN_PERIOD':{
            return {
                ...state,
                accountsInPeriod : action.accounts
            }
            break
        }

        case 'ADD_ACCOUNT':{
            
            return {
                ...state,
                accountsInPeriod : state.accountsInPeriod.concat(action.account)
            }
            break
        }

        case 'UPDATE_ACCOUNT':{
            let index = state.accountsInPeriod.map(item => item.id).indexOf(action.account.id)
            if(index >= 0){
                let newArr = state.accountsInPeriod.slice(0)
                if (action.account.finished_at === null) newArr[index].name = action.account.name
                else newArr.splice(index, 1)
                
                return{
                    ...state,
                    accountsInPeriod: newArr
                }
            }
            break
        }

        case 'DELETE_ACCOUNT':{
            let index = state.accountsInPeriod.map(item => item.id).indexOf(action.accountId)
            if(index >= 0){
                let newArr = state.accountsInPeriod.slice(0)
                newArr.splice(index, 1)
                return{
                    ...state,
                    accountsInPeriod: newArr
                }
            }
            break
        }

        case 'REFRESH_MEMBERS':{
            return {
                ...state,
                members : action.members
            }
            break
        }

        case 'ADD_MEMBER':{
            return {
                ...state,
                members : state.members.concat(action.member),
                lastMember : action.member,
                accountsInPeriod : state.accountsInPeriod.map(item => item.id == action.member.account_id ? {...item, members_count : ++item.members_count} : item)
            }
            break
        }

        case 'UPDATE_MEMBER':{
            let index = state.members.map(item => item.id).indexOf(action.member.id)
            if(index >= 0){
                let newArr = state.members.slice(0)
                newArr[index].name = action.member.name
                return{
                    ...state,
                    lastMember : action.member,
                    members: newArr
                }
            }
            break
        }

        case 'DELETE_MEMBER':{
            let index = state.members.map(item => item.id).indexOf(action.memberId)
            if(index >= 0){
                let newArr = state.members.slice(0)
                newArr.splice(index, 1)
                return{
                    ...state,
                    members: newArr
                }
            }
            break
        }

        case 'REFRESH_PRODUCTS':{
            return {
                ...state,
                products : action.products
            }
            break
        }

        case 'ADD_PRODUCT':{
            return {
                ...state,
                products : state.products.concat(action.product)
            }
            break
        }

        case 'UPDATE_PRODUCT':{
            let index = state.products.map(item => item.id).indexOf(action.product.id)
            if(index >= 0){
                let newArr = state.products.slice(0)
                newArr[index].name = action.product.name
                newArr[index].price = action.product.price
                return{
                    ...state,
                    products: newArr
                }
            }
            break
        }

        case 'DELETE_PRODUCT':{
            let index = state.products.map(item => item.id).indexOf(action.productId)
            if(index >= 0){
                let newArr = state.products.slice(0)
                newArr.splice(index, 1)
                return{
                    ...state,
                    products: newArr
                }
            }
            break
        }

        default:{
            return state
            break
        }
    }   
    
}

const logger = store => next => action => {
    let result = next(action)
    return result
}

export default createStore(reducer, { accountsInPeriod : [], members: [], products: [], lastMember : {}, accountsFinished : []}, applyMiddleware(logger, thunk))