<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    protected $table = 'tbl_debt';

    protected $fillable = ['quantity', 'member_id', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

}
