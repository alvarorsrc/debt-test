<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'tbl_account';

    protected $fillable = ['name', 'finished_at'];


    public function members()
    {
        return $this->hasMany('App\Models\Member');
    }

}
