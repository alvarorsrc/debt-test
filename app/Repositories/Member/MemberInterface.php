<?php

namespace App\Respositories\Member;

interface MemberInterface 
{
    public function getByAccountId($id);

    public function store(array $fields);

    public function update($id, array $fields);
    
    public function delete($id);
}