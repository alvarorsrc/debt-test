<?php

namespace App\Respositories\Member;

use App\Models\Member;

class MemberEloquent implements MemberInterface
{
    private $model;

    public function __construct(Member $model) {
        $this->model = $model;
    }

    public function getByAccountId($id)
    {
        $members = $this->model->where('account_id', $id)
                               ->get();
        return [
            'status' => true,
            'members' => $members
        ];
    }

    public function store(array $fields)
    {
        $member = $this->model->create($fields);
        return [
            'status' => true,
            'member' => $member
        ];
    }

    public function update($id, array $fields)
    {
        $member = $this->model->findOrFail($id);
        $member->update($fields);
        return [
            'status' => true,
            'member' => $member
        ];
    }
    
    public function delete($id)
    {
        $this->model->find($id)->delete();
        return ['status' => true];
    }
}