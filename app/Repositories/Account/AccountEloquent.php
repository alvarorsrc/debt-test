<?php

namespace App\Respositories\Account;

use App\Models\Account;

class AccountEloquent implements AccountInterface
{
    private $model;

    public function __construct(Account $model)
    {
        $this->model = $model;
    }

    public function getAllInPeriod()
    {
        $accounts = $this->model->where('finished_at', null)
                           ->withCount('members')
                           ->get();
        return [
            'status' => true,
            'accounts' => $accounts
        ];
    }

    public function getAllFinished()
    {
        $accounts = $this->model->where('finished_at', '<>', null)
                           ->orderBy('finished_at', 'DESC')
                           ->get();
        return [
            'status' => true,
            'accounts' => $accounts
        ];
    }

    public function getInPeriodById($id)
    {
        $account = $this->model->where('finished_at', null)
                           ->where('id', $id)
                           ->first();
        return [
            'status' => true,
            'account' => $account
        ];
    }

    public function getFinishedByDate($startDate = null, $endDate = null)
    {
        if ($startDate === null && $endDate === null)
        {
            return $this->getAllFinished();
        }

        $startDate = $startDate === null ? date("Y-m-d", '0000-00-00') : date("Y-m-d", strtotime($startDate)); 
        $endDate = $endDate === null ? date("Y-m-d") : date("Y-m-d", strtotime($endDate)); 
        
        $accounts = $this->model->where('finished_at', '<>', null)
                           ->where('created_at', '>=', $startDate)
                           ->where('finished_at', '<=', $endDate)
                           ->orderBy('finished_at', 'DESC')
                           ->get();
        return [
            'status' => true,
            'accounts' => $accounts
        ];
    }

    public function store(array $fields)
    {
        $account = $this->model->create($fields);
        return [
            'status' => true,
            'account' => $account
        ];
    }

    public function update($id, array $fields)
    {
        $account = $this->model->findOrFail($id);
        $account->update($fields);
        return [
            'status' => true,
            'account' => $account
        ];
    }
    
    public function delete($id)
    {
        $this->model->find($id)->delete();
        return ['status' => true];
    }
}