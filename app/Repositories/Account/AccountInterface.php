<?php

namespace App\Respositories\Account;

interface AccountInterface 
{
    //Obtiene las cuentas no finalizadas
    public function getAllInPeriod();

    //Obtiene las cuentas finalizadas
    public function getAllFinished();

    //Obtiene por ID las cuentas no finalizadas
    public function getInPeriodById($id);

    //Obtiene por rango de fecha las cuentas finalizadas
    public function getFinishedByDate($startDate, $endDate);

    public function store(array $fields);

    public function update($id, array $fields);
    
    public function delete($id);
}