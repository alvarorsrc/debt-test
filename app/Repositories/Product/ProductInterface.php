<?php

namespace App\Respositories\Product;

interface ProductInterface 
{
    public function getAll();

    public function store(array $fields);

    public function update($id, array $fields);
    
    public function delete($id);
}