<?php

namespace App\Respositories\Product;

use App\Models\Product;

class ProductEloquent implements ProductInterface
{
    private $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        $products = $this->model->all();
        return [
            'status' => true,
            'products' => $products
        ];
    }

    public function store(array $fields)
    {
        $product = $this->model->create($fields);
        return [
            'status' => true,
            'product' => $product
        ];
    }

    public function update($id, array $fields)
    {
        $product = $this->model->findOrFail($id);
        $product->update($fields);
        return [
            'status' => true,
            'product' => $product
        ];
    }
    
    public function delete($id)
    {
        $this->model->find($id)->delete();
        return ['status' => true];
    }
}