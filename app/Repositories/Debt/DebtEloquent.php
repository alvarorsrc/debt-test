<?php

namespace App\Respositories\Debt;

use App\Models\Debt;

class DebtEloquent implements DebtInterface
{
    private $model;

    public function __construct(Debt $model)
    {
        $this->model = $model;
    }

    public function getByMemberId($id)
    {
        $debts = $this->model->select('tbl_debt.id', 'tbl_debt.quantity', 'tbl_product.name as product', 'tbl_product.price as price', 'tbl_debt.product_id')
                           ->join('tbl_product', 'tbl_product.id', 'tbl_debt.product_id')
                           ->where('member_id', $id)
                           ->get();
        return [
            'status' => true,
            'debts' => $debts
        ];
    }

    public function store(array $fields)
    {
        $debt = $this->model->create($fields);
        
        return [
            'status' => true,
            'debt' => [
                'id' => $debt->id,
                'quantity' => $debt->quantity,
                'product' => $debt->product->name,
                'price' => $debt->product->price,
                'product_id' => $debt->product_id,
            ]
        ];
    }

    public function update($id, array $fields)
    {
        $debt = $this->model->findOrFail($id);
        $debt->update($fields);
        return [
            'status' => true,
            'debt' => [
                'id' => $debt->id,
                'quantity' => $debt->quantity,
                'product' => $debt->product->name,
                'price' => $debt->product->price,
                'product_id' => $debt->product_id,
            ]
        ];
    }

    public function delete($id)
    {
        $this->model->find($id)->delete();
        return ['status' => true];
    }
}