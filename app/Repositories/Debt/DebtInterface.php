<?php

namespace App\Respositories\Debt;

interface DebtInterface 
{
    public function getByMemberId($id);

    public function store(array $fields);

    public function update($id, array $fields);

    public function delete($id);
}