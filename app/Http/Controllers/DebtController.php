<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Respositories\Debt\DebtInterface;
use Exception;

class DebtController extends Controller
{
    private $debt;

    public function __construct(DebtInterface $debt){
        $this->debt = $debt;
    }

    public function index()
    {
        
    }

    public function store(Request $request)
    {
        try{
            return $this->debt->store($request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function show($id)
    {
        try{
            return $this->debt->getByMemberId($id);
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function update(Request $request, $id)
    {
        try{
            return $this->debt->update($id, $request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function destroy($id)
    {
        try{
            return $this->debt->delete($id);
        } catch(Exception $e){
            return ['status' => false];
        }
    }
}
