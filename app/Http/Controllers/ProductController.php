<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Respositories\Product\ProductInterface;
use Exception;

class ProductController extends Controller
{
    private $product;

    public function __construct(ProductInterface $product){
        $this->product = $product;
    }

    public function index()
    {
        try {
            return $this->product->getAll();
        } catch(Exception $e){
            return ['status' => false];
        }
    }
    
    public function store(Request $request)
    {
        try{
            return $this->product->store($request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function show($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        try{
            return $this->product->update($id, $request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function destroy($id)
    {
        try{
            return $this->product->delete($id);
        } catch(Exception $e){
            return ['status' => false];
        }
    }
}
