<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Respositories\Member\MemberInterface;
use Exception;

class MemberController extends Controller
{
    private $member;

    public function __construct(MemberInterface $member){
        $this->member = $member;
    }
    
    public function index()
    {
        
    }

    public function store(Request $request)
    {
        try{
            return $this->member->store($request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function show($id)
    {
        try{
            return $this->member->getByAccountId($id);
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function update(Request $request, $id)
    {
        try{
            return $this->member->update($id, $request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function destroy($id)
    {
        try{
            return $this->member->delete($id);
        } catch(Exception $e){
            return ['status' => false];
        }
    }
}
