<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Respositories\Account\AccountInterface;
use Exception;

class AccountController extends Controller
{
    private $account;

    public function __construct(AccountInterface $account){
        $this->account = $account;
    }

    public function index()
    {
        try{
            return $this->account->getAllInPeriod();
        } catch(Exception $e){
            return ['status' => false];
        }
    }
    
    public function store(Request $request)
    {
        try{
            return $this->account->store($request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function show($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        try{
            if($request->has('finish')){
                return $this->account->update($id, ['finished_at' => date('Y-m-d H:i:s')]);    
            }
            return $this->account->update($id, $request->input());
        } catch(Exception $e){
            return ['status' => false];
        }
    }

    public function destroy($id)
    {
        try{
            return $this->account->delete($id);
        } catch(Exception $e){
            return ['status' => false];
        }
    }
}
