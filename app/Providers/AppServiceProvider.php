<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Respositories\Account\AccountInterface;
use App\Respositories\Member\MemberInterface;
use App\Respositories\Product\ProductInterface;
use App\Respositories\Debt\DebtInterface;

use App\Respositories\Account\AccountEloquent;
use App\Respositories\Member\MemberEloquent;
use App\Respositories\Product\ProductEloquent;
use App\Respositories\Debt\DebtEloquent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AccountInterface::class, AccountEloquent::class);
        $this->app->singleton(MemberInterface::class, MemberEloquent::class);
        $this->app->singleton(ProductInterface::class, ProductEloquent::class);
        $this->app->singleton(DebtInterface::class, DebtEloquent::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
